﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour {

	public FightManager _fightManager;
	public string Name;

	public int attack;
	public int defense;
	public int initiative;
	public int damages;
	public int maximumLife;
	public int currentLife;

	public int currentInitiative;
	public int counterBonus;
	public Character target;

	public bool canAttack = true;
	public int CurrentAttackNumber = 0;
	public int MaxAttackNumber = 1;

	public virtual void RoundReset()
	{
		//reset du bonus de contre attaque
		counterBonus = 0;
		canAttack = true;
		CurrentAttackNumber = MaxAttackNumber;
	}

	//Calculer l'initiative pour savoir qui agit en premier
	public virtual void CalculateInitiative()
	{
		// le personnage fait un jet d'initiave pour ce round
		currentInitiative = initiative + GetDice100Result();
		//afficher le resultat dans la console
		MyLog(Name+" initiative "+ currentInitiative);
	}

	public virtual int GetDice100Result()
	{
		return Random.Range(1,100);
	}

	public virtual void Reset()
	{
		currentLife = maximumLife;
	}

	public virtual void Attack()
	{
		CurrentAttackNumber --;
		//annoncer dans la console que le personnage attaque
		MyLog(Name+" attaque "+target.Name+".");
		//calculer le resultat d'attaque ( = Attaque + nombre aléatoire entre 1 et 100 ) et l'envoyer à l'adversaire
		target.Defend(attack+GetDice100Result(),damages,this);
	}

	public virtual void Counter(int _CounterBonus, Character Attacker)
	{
		CurrentAttackNumber --;
		//annoncer dans la console que le personnage contre-attaque
		MyLog(Name+" contre-attaque sur "+Attacker.Name+".");
		//le personnage fait un jet d'Attaque. Le résultat est envoyé à l'adversaire
		Attacker.Defend(attack+GetDice100Result()+_CounterBonus,damages,this);
	}
		
	public virtual void Defend(int _attack, int _damage, Character Attacker, bool canBeCountered = true)
	{
		//On calcule la marge d'attaque
		//en soustrayant le jet de defense du personnage qui defend au jet d'attaque reçu
		int AttaqueMargin = _attack-(defense+GetDice100Result());
		//Si la marge d'attaque est supérieure à 0
		if(AttaqueMargin > 0)
		{
			MyLog(Name+" se defend mais encaisse quand même le coup.");
			//on calcule les dégâts finaux
			int finalDamages = (int)(AttaqueMargin * _damage/100f);
			//le defenseur subit des dégats
			TakeDamages(finalDamages);
		}
		else{
			//annoncer dans la console que le personnage a reussi sa defense
			MyLog(Name+" réussi sa défense.");
			if(Attacker != null && canAttack && canBeCountered && CurrentAttackNumber > 0)
			{
				Counter(-AttaqueMargin,Attacker);
			}
		}
	}

	public virtual void TakeDamages(int _damages)
	{
		MyLog(Name+" subis "+_damages+" points de dégats.");
		currentLife -= _damages;
		canAttack = false;
		if(currentLife <= 0)
		{
			MyLog(Name+" est mort.");
		}
	}

	//selectionner une cible valide
	public virtual void SelectTargetAndAttack()
	{
		target = null;
		//on cree une liste dans laquelle on stockera les cibles valides
		List<Character> validTarget = new List<Character>();

		for(int i = 0; i <  _fightManager.charactersList.Count; i++)
		{
			Character currentCharacter =  _fightManager.charactersList[i];
			//si le personnga etesté n'est pas celui qui attaque et qu'il est vivant
			if(currentCharacter != this && currentCharacter.currentLife > 0)
			{
				//on l'ajoute à la liste des cible valide
				validTarget.Add(currentCharacter);
			}
		}

		if(validTarget.Count > 0)
		{
			//on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque 
			target = validTarget[Random.Range(0,validTarget.Count)];
			Attack();
		}
		else
		{
			MyLog(Name+" n'a pas trouvé de cible valide");
			CurrentAttackNumber = 0;
		}
	}

	public void MyLog(string text)
	{
		Debug.Log(text);
	}

}
