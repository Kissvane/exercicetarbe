﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FightManager : MonoBehaviour {

	//liste des personnage participant au combat
	public List<Character> startFighterList;
	public List<Character> charactersList = new List<Character>();
	public int round = 0;

	public void Start()
	{
		//commencer le combat
		StartCombat();
	}

	public void StartCombat()
	{
		round = 0;
		charactersList.Clear();
		charactersList.AddRange(startFighterList);
		for(int i = 0; i < charactersList.Count; i++)
		{
			charactersList[i]._fightManager = this;
		}
		//faire en sorte que les personnages ne soient pas blessé avant le début du combat
		foreach(Character personnage in charactersList)
		{
			personnage.Reset();
		}
		MyLog("----- Debut du combat -----");
		//a commenter pour enchainer les rounds à la main
		//faire des rounds tant qu'il y a plus d'un combattant vivant
		while(charactersList.Count > 1)
		{
			StartRound();
		}
	}

	public void StartRound()
	{
		round ++;
		MyLog("---------- Round "+round+" ----------");

		foreach(Character p in charactersList)
		{
			//reinitialiser les variables nécessaires
			p.RoundReset();
			//calculer l'initiative pour ce round
			p.CalculateInitiative();
		}

		//classer les différents personnage en fonction de initiative
		charactersList = charactersList.OrderByDescending(personnage => personnage.currentInitiative).ToList();

		for(int i = 0; i < charactersList.Count; i++)
		{
			//on stocke le personnage dans une variable pour éviter d'accéder inutilement à la liste
			Character currentPersonnage = charactersList[i];
			//si le personnage peut attaquer et qu'il est vivant
			if(currentPersonnage.canAttack && currentPersonnage.CurrentAttackNumber > 0 && currentPersonnage.currentLife > 0)
			{
				while(currentPersonnage.CurrentAttackNumber > 0 && currentPersonnage.canAttack)
				{
					//choisir une cible puis attaquer
					currentPersonnage.SelectTargetAndAttack();
				}
			}
			else if(currentPersonnage.CurrentAttackNumber == 0 && currentPersonnage.currentLife > 0)
			{
				MyLog(currentPersonnage.Name+" n'a plus d'attaques disponibles et ne peut rien faire pendant ce round.");
			}
			else if(!currentPersonnage.canAttack && currentPersonnage.currentLife > 0)
			{
				MyLog(currentPersonnage.Name+" a subis des degats et ne peut rien faire pendant ce round.");
			}
		}
		//on fait une deuxième boucle sur les personnage pour retirer les morts de la liste
		//on fait cette boucle de la fin vers le début pour éviter les problèmes que l'on rencontre quand on modifie 
		//une liste sur laquelle on est en train d'itérer
		for(int i = charactersList.Count-1; i >= 0; i--)
		{
			//on stocke le personnage dans une variable pour éviter d'accéder inutilement à la liste
			Character currentPersonnage = charactersList[i];
			if(currentPersonnage.currentLife <= 0)
			{
				charactersList.Remove(currentPersonnage);
			}
		}

		MyLog("---------- Fin du round ----------");

		if(charactersList.Count == 1)
		{
			Debug.Log(charactersList[0].Name+" remporte le battle royale");
		}
		else if(charactersList.Count == 0)
		{
			Debug.Log("Il n'y a pas de survivant donc pas de gagnant");
		}
	}

	public void MyLog(string text)
	{
		Debug.Log(text);
	}
}
