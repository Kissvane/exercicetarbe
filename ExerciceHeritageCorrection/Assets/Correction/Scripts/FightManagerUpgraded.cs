﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FightManagerUpgraded : MonoBehaviour {

	//liste des personnage participant au combat
	public List<CharacterUpgraded> startFighterList;
	public List<CharacterUpgraded> charactersList = new List<CharacterUpgraded>();
	public int round = 0;
	public float startTime = 0f; 
	public bool reported = false;
	public WinCounter _winCounter;
	public int StartNumberFighter = 0;

	public void Start()
	{
		//commencer le combat
		StartCombat();
	}

	public void StartCombat()
	{
        //Initialisation du combat
		startTime = Time.time; 
		round = 0;
		reported = false;
		charactersList.Clear();
		charactersList.AddRange(startFighterList);
		for(int i = 0; i < charactersList.Count; i++)
		{
			charactersList[i]._fightManagerUpgraded = this;
		}
		StartNumberFighter = startFighterList.Count;
		//faire en sorte que les personnages ne soient pas blessé avant le début du combat
		foreach(CharacterUpgraded personnage in charactersList)
		{
			personnage.Reset();
		}
		MyLog("----- Debut du combat -----");
		//a commenter pour enchainer les rounds à la main
		//faire des rounds tant qu'il y a plus d'un combattant vivant
		while(charactersList.Count > 1)
		{
			StartRound();
		}
	}

	//a decommenter pour enchainer les rounds à la main 
	/*void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space) && charactersList.Count > 1)
		{
			StartRound();
		}
	}*/

	public void StartRound()
	{
		round ++;
		MyLog("---------- Round "+round+" ----------");

		foreach(CharacterUpgraded p in charactersList)
		{
			//reinitialiser les variables nécessaires
			p.RoundReset();
			//calculer l'initiative pour ce round
			p.CalculateInitiative();
		}

		//classer les différents personnage en fonction de initiative
		charactersList = charactersList.OrderByDescending(personnage => personnage.currentInitiative).ToList();

		for(int i = 0; i < charactersList.Count; i++)
		{
			//on stocke le personnage dans une variable pour éviter d'accéder inutilement à la liste
			CharacterUpgraded currentPersonnage = charactersList[i];
			//si le personnage peut attaquer et qu'il est vivant
			if(currentPersonnage.canAttack && currentPersonnage.CurrentAttackNumber > 0 && currentPersonnage.currentLife > 0)
			{
				while(currentPersonnage.CurrentAttackNumber > 0 && currentPersonnage.canAttack)
				{
					//choisir une cible puis attaquer
					currentPersonnage.SelectTargetAndAttack();
				}
			}
			else if(currentPersonnage.CurrentAttackNumber == 0 && currentPersonnage.currentLife > 0)
			{
				MyLog(currentPersonnage.Name+" n'a plus d'attaques disponibles et ne peut rien faire pendant ce round.");
			}
			else if(!currentPersonnage.canAttack && currentPersonnage.currentLife > 0)
			{
				MyLog(currentPersonnage.Name+" a subis des degats et ne peut rien faire pendant ce round.");
			}
		}
		//on fait une deuxième boucle sur les personnage pour retirer les morts de la liste
		//on fait cette boucle de la fin vers le début pour éviter les problèmes que l'on rencontre quand on modifie 
		//une liste sur laquelle on est en train d'itérer
		for(int i = charactersList.Count-1; i >= 0; i--)
		{
			//on stocke le personnage dans une variable pour éviter d'accéder inutilement à la liste
			CharacterUpgraded currentPersonnage = charactersList[i];
            //si le personnage est mort
            if (currentPersonnage.currentLife <= 0)
			{
                //on le retire de la liste
				charactersList.Remove(currentPersonnage);
			}
		}

		MyLog("---------- Fin du round ----------");
        //si il ne reste qu'un personnage vivant, on le déclare vainqueur
		if(charactersList.Count == 1)
		{
			_winCounter.SetPoints(StartNumberFighter,charactersList[0].Name);
			_winCounter.SetVictory(charactersList[0].Name);
			MyLog(charactersList[0].Name+" remporte le battle royale");
		}
        //si il n'y a pas de survivant on déclare le match nul
		else if(charactersList.Count == 0)
		{
			MyLog("Il n'y a pas de survivant donc pas de gagnant");
		}
	}

	//informer tous les survivants qu'un combattant est mort
	public void SomebodyIsDead(CharacterUpgraded deadMan)
	{
        //on creer une variable pour compter le nomvre de personnages vivants 
		int aliveCharacters = 0;
		for(int i = 0; i < charactersList.Count; i++)
		{
			//on stocke le personnage dans une variable pour éviter d'accéder inutilement à la liste
			CharacterUpgraded currentPersonnage = charactersList[i];
            //si le personnage est vivant
            if (currentPersonnage.currentLife > 0)
			{
                //on augmente le nomber de personnages vivants
				aliveCharacters++;
                //on l'informe qu'un personnage est mort
				currentPersonnage.SomebodyIsDead(deadMan);
			}
		}
        //partie du code utilisée pour faire de la statistic
		int points = StartNumberFighter - aliveCharacters - 1;
		_winCounter.SetPoints(points,deadMan.Name);
	}

    //affiche tous les messages liés au combat
	public void MyLog(string text)
	{
		Debug.Log(text);
	}
}
