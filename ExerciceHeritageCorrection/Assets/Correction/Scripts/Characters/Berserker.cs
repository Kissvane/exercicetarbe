﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Berserker : CharacterUpgraded
{
    int BerserkerBonus = 0;
    int baseDamages = 0;
    int baseInitiative = 0;
    int baseAttack = 0;
    int baseMaxAttackNumber = 0;

    public void Awake()
    {
        baseAttack = attack;
        baseInitiative = initiative;
        baseDamages = damages;
        baseMaxAttackNumber = MaxAttackNumber;
    }

    public override void Reset()
    {
        currentLife = maximumLife;
        poisonLevel = 0;
        attack = baseAttack;
        initiative = baseInitiative;
        damages = baseDamages;
        MaxAttackNumber = baseMaxAttackNumber;
    }

    public override void TakeDamages(int _damages, CharacterUpgraded Attacker)
    {
        MyLog(Name + " subis " + _damages + " points de dégats.");
        if (Hidden) Reveal();
        currentLife -= _damages;
        if (currentLife <= damages)
        {
            canAttack = false;
        }

        int BerserkerBonus = Mathf.RoundToInt((maximumLife-currentLife) / 3f);

        damages = baseDamages + BerserkerBonus;
        initiative = baseInitiative + BerserkerBonus;
        attack = baseAttack + BerserkerBonus;

        int AttackNumberBonus = Mathf.RoundToInt((1 - ((float)currentLife / (float)maximumLife)) * 10f);
        MaxAttackNumber = baseMaxAttackNumber + AttackNumberBonus;

        if (currentLife <= 0)
        {
            MyLog(Name + " est mort.");
            _fightManagerUpgraded.SomebodyIsDead(this);
        }
        if (Attacker != null) Attacker.OpponentHurted(_damages, this);
    }


}
