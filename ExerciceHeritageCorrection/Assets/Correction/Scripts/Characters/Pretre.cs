﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pretre : CharacterUpgraded {

	public override void RoundReset()
	{
        //soin de 10%
        Heal(maximumLife / 10);
        //reset du personnage pour le round suivant
        base.RoundReset();
	}

	//selectionner une cible valide
	public override void SelectTargetAndAttack()
	{
		target = null;
		//on cree une liste dans laquelle on stockera les cibles valides
		List<CharacterUpgraded> validTarget = new List<CharacterUpgraded>();
		List<CharacterUpgraded> prioritaryTarget = new List<CharacterUpgraded>();
        //on pârc
        for (int i = 0; i < _fightManagerUpgraded.charactersList.Count; i++)
		{
			CharacterUpgraded currentCharacter = _fightManagerUpgraded.charactersList[i];
			//si le personnga testé n'est pas celui qui attaque et qu'il est vivant et qu'il n'est pas camouflé
			if(currentCharacter != this && currentCharacter.currentLife > 0 && !currentCharacter.Hidden)
			{
				//si la cible est un mort vivant on l'ajoute à la liste des cibles prioritaires
				if(currentCharacter.undead) prioritaryTarget.Add(currentCharacter);
                //on l'ajoute à la liste des cible valide
                else validTarget.Add(currentCharacter);

			}
		}
        //si il y a au moins une cible prioritaire
		if(prioritaryTarget.Count > 0)
		{
            //on prend un personngae au hasard dans la liste des cibles prioritaires et on le designe comme la cible de l'attaque 
            target = prioritaryTarget[Random.Range(0,prioritaryTarget.Count)];
            //attaquer la cible
			Attack();
		}
		else if(validTarget.Count > 0)
		{
			//on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque 
			target = validTarget[Random.Range(0,validTarget.Count)];
            //attaquer la cible
            Attack();
		}
        //si on ne trouve pas de cible valide
        else
        {
			MyLog(Name+" n'a pas trouvé de cible valide");
            //on réduit le nombre d'attaque restant du persnnage à 0 pour éviter la boucle infinie
            CurrentAttackNumber = 0;
		}

	}
}
