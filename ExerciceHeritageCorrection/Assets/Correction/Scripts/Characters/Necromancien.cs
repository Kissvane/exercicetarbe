﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Necromancien : CharacterUpgraded 
{
    //variables permettant de sauvegarder les caracteristiques initiales du nécromancien
	public int baseDefense;
	public int baseAttack;
	public int baseDamages;
	public int baseMaximumLife;
	public int baseMaxAttackNumber;
	public int Deads;

    //enregistrement des caracteristiques initiales
	public void Awake()
	{
		baseDefense = defense;
		baseAttack = attack;
		baseDamages = damages;
		baseMaximumLife = maximumLife;
		baseMaxAttackNumber = MaxAttackNumber;
	}

    //réinitialisation du personnage
	public override void Reset ()
	{
		defense = baseDefense;
		attack = baseAttack;
		damages = baseDamages;
		maximumLife = baseMaximumLife;
		MaxAttackNumber = baseMaxAttackNumber;
		Deads = 0;
		base.Reset ();
	}

    //Réinitialiser les variables nécessaire pour le bon déroulement du nouveau round
	public override void RoundReset()
	{
        //executer la fonction de la classe mère
		base.RoundReset();
        //si il y a 5 combattanst ou plus et que personne n'est mort
		if(_fightManagerUpgraded.charactersList.Count >= 5 && Deads == 0)
		{
            //cacher le personnage
            Hide();
		}
	}

	public override void SomebodyIsDead(CharacterUpgraded deadMan)
	{
        //executer la fonction de base
		base.SomebodyIsDead(deadMan);
        //augmenter le compteur de mort
		Deads++;
        //augmentation des caractéristiques du nécromancien
		SoulAbsorption(deadMan);
	}

    //augmentation des caractéristiques du nécromancien quand un combattant meurt
    public void SoulAbsorption(CharacterUpgraded deadMan)
	{
		MyLog(Name +" absorbe l'âme de "+deadMan.Name+" et ses pouvoirs se renforcent.");
		attack += 5;
		defense += 5;
		damages += 5;
		maximumLife += 50;
		currentLife += 50;
		MaxAttackNumber += 1;
		CurrentAttackNumber += 1;
	}
}
