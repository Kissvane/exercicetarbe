﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assassin : CharacterUpgraded 
{
    //rest le personnage pour le round
	public override void RoundReset()
	{
		base.RoundReset();
        //si il y au moins 5 combattants
		if(_fightManagerUpgraded.charactersList.Count >= 5)
		{
            //se camoufler
			Hide();
		}
	}

	public override void OpponentHurted (int _damages, CharacterUpgraded HurtedMan)
	{
        //si les dégats infligés par l'assassin sont supérieurs à la moitié de la vie de la cible
		if(_damages >= HurtedMan.currentLife)
		{
			MyLog(Name+" a touché un point vital, "+HurtedMan.Name+" meurt sur le coup.");
            //la cible meurt
			HurtedMan.currentLife = 0;
		}
	}
}
