﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gardien : CharacterUpgraded {

	public override void Counter(int _CounterBonus, CharacterUpgraded Attacker)
	{
		int doubledBonus = counterBonus * 2;
		MyLog(Name+" double son bonus de contre-attaque.");
		base.Counter(doubledBonus,Attacker);
	}
}
