﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Illusioniste : CharacterUpgraded {

	public bool HasBeenHurted = false;

	public override void RoundReset()
	{
		base.RoundReset();
		HasBeenHurted = false;
		canAttack = false;
	}

	public override void TakeDamages(int _damages, CharacterUpgraded Attacker)
	{
		base.TakeDamages(_damages,Attacker);
		HasBeenHurted = true;
	}

	public override void Defend(int _attack, int _damage, CharacterUpgraded Attacker, bool canBeCountered = true)
	{
		//On calcule la marge d'attaque
		//en soustrayant le jet de defense du personnage qui defend au jet d'attaque reçu
		int AttaqueMargin = _attack-(defense+GetDice100Result());
		//ReduceStamina();
		//Si la marge d'attaque est supérieure à 0
		if(AttaqueMargin > 0)
		{
			MyLog(Name+" se defend mais encaisse quand même le coup.");
			//on calcule les dégâts finaux
			int finalDamages = (int)(AttaqueMargin * _damage/100f);
			//le defenseur subit des dégats
			if(Attacker.realDamagesPercentage > 0) TakeDamages((int)(finalDamages*Attacker.realDamagesPercentage/100f),Attacker);
			//Les morts-vivants sont immunisés au poison
			if(Attacker.poisoningPercentage > 0 && !undead) AugmentPoisonLevel((int)(finalDamages*Attacker.poisoningPercentage/100f));
		}
		else{
			//annoncer dans la console que le personnage a reussi sa defense
			MyLog(Name+" réussi sa défense.");
			if(Attacker != null && !HasBeenHurted && canBeCountered)
			{
				Counter(-AttaqueMargin,Attacker);
			}
		}
	}
}
