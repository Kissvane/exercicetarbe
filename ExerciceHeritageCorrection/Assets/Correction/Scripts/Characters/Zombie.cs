﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : CharacterUpgraded {

	public override void Defend(int _attack, int _damage, CharacterUpgraded Attacker, bool canBeCountered = true)
	{
		//On calcule la marge d'attaque
		//en soustrayant le jet de defense du personnage (0 pour le zombie) qui defend au jet d'attaque reçu
		int AttaqueMargin = _attack-0;
		//on calcule les dégâts finaux
		int finalDamages = (int)(AttaqueMargin * _damage/100f);
		if(Attacker.holyDamages && undead) finalDamages *= 2;
		//le defenseur subit des dégats
		if(Attacker.realDamagesPercentage > 0) TakeDamages((int)(finalDamages*Attacker.realDamagesPercentage/100f),Attacker);
		//Les morts-vivants sont immunisés au poison
		if(Attacker.poisoningPercentage > 0 && !undead) AugmentPoisonLevel((int)(finalDamages*Attacker.poisoningPercentage/100f));
		//ReduceStamina();
	}

	public override void TakeDamages(int _damages, CharacterUpgraded Attacker)
	{
		MyLog(Name+" encaisse le coup et subis "+_damages+" points de dégats.");
		currentLife -= _damages;
		if(currentLife <= 0)
		{
			MyLog(Name+" est mort.");
			_fightManagerUpgraded.SomebodyIsDead(this);
		}
		if(Attacker != null) Attacker.OpponentHurted(_damages,this);
	}
}
