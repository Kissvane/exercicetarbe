﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : CharacterUpgraded {

	public int baseAttack;

	public void Awake()
	{
		baseAttack = attack;
	}

	public override void Reset ()
	{
		attack = baseAttack;
		base.Reset ();
	}

	public override void RoundReset()
	{
		base.RoundReset();
		attack = attack + (int)(attack*0.5f);
        MyLog("L'attaque de " + Name + " augmente à "+attack+".");
    }
}
