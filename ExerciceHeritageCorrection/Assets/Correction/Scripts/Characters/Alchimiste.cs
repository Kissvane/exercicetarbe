﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alchimiste : CharacterUpgraded {

	public void Attack(int _AttackRoll)
	{
		if(Hidden) Reveal();
		//annoncer dans la console que le personnage attaque
		//calculer le resultat d'attaque ( = Attaque + nombre aléatoire entre 1 et 100 ) et l'envoyer à l'adversaire
		target.Defend(_AttackRoll,damages,this,false);
	}

	public override void Counter(int _CounterBonus, CharacterUpgraded Attacker)
	{
		if(Hidden) Reveal();
		CurrentAttackNumber --;
		//annoncer dans la console que le personnage contre-attaque
		MyLog(Name+" contre-attaque sur "+Attacker.Name+".");
        target = Attacker;
        //le personnage fait un jet d'Attaque.
        int AttackRoll = attack + GetDice100Result();
        //Le résultat est envoyé à l'adversaire
        Attacker.Defend(AttackRoll+_CounterBonus,damages,this,false);
        //ReduceStamina();

        MyLog(Name + " attaque tout le monde avec un nuage empoisonné.");
        for (int i = 0; i < _fightManagerUpgraded.charactersList.Count; i++)
        {
            CharacterUpgraded currentCharacter = _fightManagerUpgraded.charactersList[i];
            //si le personnage testé n'est pas celui qui attaque et qu'il est vivant et qu'il n'est pas caché
            if (currentCharacter != this && currentCharacter.currentLife > 0 && currentCharacter != target)
            {
                //definir le personnage comme la cible
                target = currentCharacter;
                //attaquer la cible avec la meme valeur que l'attaque initiale mais sans compter le bonsu de contre attaque
                target.Defend(AttackRoll, damages, this, false);
            }
        }
    }

	//selectionner une cible valide
	public override void SelectTargetAndAttack()
	{
		target = null;
		MyLog(Name+" attaque tout le monde avec un nuage empoisonné.");
        //le personnage fait un jet d'Attaque.
        int AttackRoll = attack + GetDice100Result();
        for (int i = 0; i <  _fightManagerUpgraded.charactersList.Count; i++)
		{
			CharacterUpgraded currentCharacter =  _fightManagerUpgraded.charactersList[i];
			//si le personnage testé n'est pas celui qui attaque et qu'il est vivant et qu'il n'est pas caché
			if(currentCharacter != this && currentCharacter.currentLife > 0)
			{
                //definir le personnage comme la cible
				target = currentCharacter;
                //attaquer la cible
				Attack(AttackRoll);
			}
		}
        //si on ne trouve pas de cible valide
        if (target == null)
		{
			MyLog(Name+" n'a pas trouvé de cible valide");
            //on réduit le nombre d'attaque restant du persnnage à 0 pour éviter la boucle infinie
            CurrentAttackNumber = 0;
		}
        //si une cible a été trouvé
		else
		{
            //réduire le nombre d'attaque
			CurrentAttackNumber --;
		}
	}
}
