﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vampire : CharacterUpgraded {

	public override void OpponentHurted(int _damages, CharacterUpgraded HurtedMan)
	{
        MyLog(Name + " suce le sang");
        Heal((int)((float)_damages / 2f));
	}
}
