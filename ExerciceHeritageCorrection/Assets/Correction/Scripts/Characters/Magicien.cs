﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magicien : CharacterUpgraded {

	public int concentrationTime = 10;
	public int multiplierBonus = 5;
	public int RoundNumber = 0;
	public int baseAttack;
	public int baseDamages;
	CharacterUpgraded lastTarget;
	//le nombre de personnages déjà touchés par l'eclair
	public int touchedCharacters = 0;

	public void Awake()
	{
		baseAttack = attack;
		baseDamages = damages;
	}

	public override void RoundReset()
	{
		base.RoundReset();

		touchedCharacters = 0;
		RoundNumber ++;

		if(RoundNumber % concentrationTime == 0)
		{
			MyLog(Name+" a fini de se concentrer, son attaque et ses dégâts sont multipliés par "+multiplierBonus);
			attack = baseAttack * multiplierBonus;
			damages = baseDamages * multiplierBonus;
		}
		else if(attack != baseAttack)
		{
			MyLog("La concentration de "+Name+"prend fin son attaque et ses dégâts redevienne normaux.");
			attack = baseAttack;
			damages = baseDamages;
		}
	}

	public override void Reset ()
	{
		base.Reset ();
		attack = baseAttack;
		damages = baseDamages;
	}

	public override void OpponentHurted(int _damages, CharacterUpgraded HurtedMan)
	{
		touchedCharacters ++;
		if(touchedCharacters < 10)
		{
			SecondaryAttack();
		}
		else
		{
			MyLog("L'éclair de "+name+" se dissipe car il a épuisé toute sa puissance.");
		}
	}

	public void SecondaryAttack()
	{
		target = null;
		//on cree une liste dans laquelle on stockera les cibles valides
		List<CharacterUpgraded> validTarget = new List<CharacterUpgraded>();
		for(int i = 0; i < _fightManagerUpgraded.charactersList.Count; i++)
		{
			CharacterUpgraded currentCharacter = _fightManagerUpgraded.charactersList[i];
			//si le personnga etesté n'est pas celui qui attaque et qu'il est vivant
			if(currentCharacter != this && currentCharacter != lastTarget && currentCharacter.currentLife > 0)
			{
				//on l'ajoute à la liste des cible valide
				validTarget.Add(currentCharacter);
			}
		}

		if(validTarget.Count > 0)
		{
			//on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque 
			target = validTarget[Random.Range(0,validTarget.Count)];
			lastTarget = target;
			//annoncer dans la console que le personnage attaque
			MyLog("L'éclair de "+Name+" continue son chemin et attaque "+target.Name);
			//calculer le resultat d'attaque ( = Attaque + nombre aléatoire entre 1 et 100 ) et l'envoyer à l'adversaire
			target.Defend(attack+GetDice100Result(),(int)(damages*touchedCharacters/10f),this,false);
		}
		else
		{
			MyLog("L'éclair de "+Name+" ne trouve plus de cible et disparait.");
		}

	}

	//selectionner une cible valide
	public override void SelectTargetAndAttack()
	{
		target = null;
		touchedCharacters = 0;
		//on cree une liste dans laquelle on stockera les cibles valides
		List<CharacterUpgraded> validTarget = new List<CharacterUpgraded>();
		for(int i = 0; i < _fightManagerUpgraded.charactersList.Count; i++)
		{
			CharacterUpgraded currentCharacter = _fightManagerUpgraded.charactersList[i];
			//si le personnga testé n'est pas celui qui attaque et qu'il est vivant
			if(currentCharacter != this && currentCharacter.currentLife > 0 && !currentCharacter.Hidden)
			{
				//on l'ajoute à la liste des cible valide
				validTarget.Add(currentCharacter);
			}
		}

		if(validTarget.Count > 0)
		{
			//on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque 
			target = validTarget[Random.Range(0,validTarget.Count)];
			lastTarget = target;
			Attack();
		}
		else
		{
			MyLog(Name+" n'a pas trouvé de cible valide");
			CurrentAttackNumber = 0;
		}

	}
}
