﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class CountCouple
{
	public string CharacterName = null;
	public int victory = 0;
	public float victoryPercentage = 0;
	public int points = 0;
	public float pointsPercentage = 0;
}

//91 points dans un combat
public class WinCounter : MonoBehaviour {

	//utiliser un dictionnaire pour les opérations et la liste a la fin

	public FightManagerUpgraded _manager;
	public List<CountCouple> stats = new List<CountCouple>();
	public int FightNumber = 1000;
	public int currentFight = 0;

	public void Start()
	{
		/*for(int i = 0; i < FightNumber; i++)
		{
			_manager.StartCombat();
		}
		stats = stats.OrderByDescending(x => x.victory).ThenByDescending(x => x.points).ToList();

		int pointsInOneFight = (_manager.StartNumberFighter*(_manager.StartNumberFighter+1))/2;

		for(int i = 0; i < stats.Count; i++)
		{
			CountCouple current = stats[i];
			current.pointsPercentage = ((float)current.points*100f)/((float)pointsInOneFight*FightNumber);
			current.victoryPercentage = (float)current.victory * 100f / (float)FightNumber;
		}*/
	}

	[ContextMenu("GOGOGO")]
	void StartCalcul()
	{
		StartCoroutine(Calculate());
	}

	IEnumerator Calculate()
	{
		for(int i = 0; i < FightNumber; i++)
		{
			_manager.StartCombat();
			currentFight++;
			//Debug.Log(currentFight);
			yield return null;
		}
		stats = stats.OrderByDescending(x => x.victory).ThenByDescending(x => x.points).ToList();

		int pointsInOneFight = (_manager.StartNumberFighter*(_manager.StartNumberFighter+1))/2;

		for(int i = 0; i < stats.Count; i++)
		{
			CountCouple current = stats[i];
			current.pointsPercentage = ((float)current.points*100f)/((float)pointsInOneFight*FightNumber);
			current.victoryPercentage = (float)current.victory * 100f / (float)FightNumber;
		}
		yield return null;
	}

	public void SetPoints(int points, string name)
	{
		/*CountCouple initialized = null;
		//chercher un slot deja initialise
		initialized = SearchInitialized(name);
		if(initialized == null)
		{
			//initialiser un slot libre
			initialized = SearchFree(name);
		}
		//ajouter les points
		initialized.points += points;*/
	}

	public void SetVictory(string name)
	{
		CountCouple initialized = null;
		//chercher un slot deja initialise
		initialized = SearchInitialized(name);
		if(initialized == null)
		{
			//initialiser un slot libre
			initialized = SearchFree(name);
		}
		//ajouter les points
		initialized.victory ++;
	}

	public CountCouple SearchInitialized(string name)
	{
		for(int i = 0; i < stats.Count; i++)
		{
			if(stats[i].CharacterName == name) return stats[i];
		}
		return null;
	}

	public CountCouple SearchFree(string name)
	{
		for(int i = 0; i < stats.Count; i++)
		{
			CountCouple current = stats[i];
			if(current.CharacterName == null)
			{
				current.CharacterName = name;
				return current;
			}
		}
		CountCouple Created = new CountCouple();
		Created.CharacterName = name;
		stats.Add(Created);
		return Created;
	}
}
