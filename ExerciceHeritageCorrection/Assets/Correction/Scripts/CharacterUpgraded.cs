﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterUpgraded : MonoBehaviour {

	public FightManagerUpgraded _fightManagerUpgraded;
	public string Name;

	public int attack;
	public int defense;
	public int initiative;
	public int damages;
	public int maximumLife;
	public int currentLife;

	public int currentInitiative;
	public int counterBonus;
	public CharacterUpgraded target;

	public bool canAttack = true;
	public int CurrentAttackNumber = 0;
	public int MaxAttackNumber = 1;

	public bool holyDamages = false;
	public bool undead = false;

	public int poisonLevel = 0;
	public int poisoningPercentage = 0;
	public int realDamagesPercentage = 100;
	public bool Hidden = false;

    public int MinDiceResult = 1;
    public int MaxDiceResult = 100;

	public Color myColor;

    public virtual void Heal(int healAmount)
    {
        currentLife += healAmount;
        currentLife = Mathf.Min(currentLife, maximumLife);
        MyLog(Name + " se soigne et sa vie remonte à " + currentLife);
    }

    //réinitialiser le personnage pour un nouveau round
	public virtual void RoundReset()
	{
		//reset du bonus de contre attaque
		counterBonus = 0;
		canAttack = true;
		CurrentAttackNumber = MaxAttackNumber;
		if(poisonLevel > 0) TakePoisonDamages();
		if(_fightManagerUpgraded.charactersList.Count < 5 && Hidden)
		{
			Reveal();
		}
	}

	//Calculer l'initiative pour savoir qui agit en premier
	public virtual void CalculateInitiative()
	{
		// le personnage fait un jet d'initiave pour ce round
		currentInitiative = initiative + GetDice100Result();
		//afficher le resultat dans la console
		MyLog(Name+" initiative "+ currentInitiative);
	}

    //retourner un nombre entre 1 et 100
	public virtual int GetDice100Result()
	{
		return Random.Range(MinDiceResult,MaxDiceResult+1);
	}

    //réinitialiser le personnage pour un nouveau combat
	public virtual void Reset()
	{
		currentLife = maximumLife;
		poisonLevel = 0;
	}

	public virtual void Attack()
	{
		if(Hidden) Reveal();
		CurrentAttackNumber --;
		//annoncer dans la console que le personnage attaque
		MyLog(Name+" attaque "+target.Name+".");
		//calculer le resultat d'attaque ( = Attaque + nombre aléatoire entre 1 et 100 ) et l'envoyer à l'adversaire
		target.Defend(attack+GetDice100Result(),damages,this);
	}

	public virtual void Counter(int _CounterBonus, CharacterUpgraded Attacker)
	{
		if(Hidden) Reveal();
		CurrentAttackNumber --;
		//annoncer dans la console que le personnage contre-attaque
		MyLog(Name+" contre-attaque sur "+Attacker.Name+".");
		//le personnage fait un jet d'Attaque. Le résultat est envoyé à l'adversaire
		Attacker.Defend(attack+GetDice100Result()+_CounterBonus,damages,this);
	}
		
	public virtual void Defend(int _attack, int _damage, CharacterUpgraded Attacker, bool canBeCountered = true)
	{
		//On calcule la marge d'attaque
		//en soustrayant le jet de defense du personnage qui defend au jet d'attaque reçu
		int AttaqueMargin = _attack-(defense+GetDice100Result());
		//Si la marge d'attaque est supérieure à 0
		if(AttaqueMargin > 0)
		{
			MyLog(Name+" se defend mais encaisse quand même le coup.");
			//on calcule les dégâts finaux
			int finalDamages = (int)(AttaqueMargin * _damage/100f);
			if(Attacker.holyDamages && undead) finalDamages *= 2;
			//le defenseur subit des dégats
			if(Attacker.realDamagesPercentage > 0) TakeDamages((int)(finalDamages*Attacker.realDamagesPercentage/100f),Attacker);
			//Les morts-vivants sont immunisés au poison
			if(Attacker.poisoningPercentage > 0 && !undead && currentLife > 0) AugmentPoisonLevel((int)(finalDamages*Attacker.poisoningPercentage/100f));
		}
		else{
			//annoncer dans la console que le personnage a reussi sa defense
			MyLog(Name+" réussi sa défense.");
            //si il y a un attaquant et que le personnage peut attaquer 
            //et que l'attaque peut-être contrée et qu'il reste des attaques au personnage
			if(Attacker != null && canAttack && canBeCountered && CurrentAttackNumber > 0)
			{
                //contre-attaquer
				Counter(-AttaqueMargin,Attacker);
			}
		}
	}

    //gestion de la prise en compte des dégâts reçus
	public virtual void TakeDamages(int _damages, CharacterUpgraded Attacker)
	{
		MyLog(Name+" subis "+_damages+" points de dégats.");
        //si le personnage est camouflé, le réveler
        if (Hidden) Reveal();
        //diminuer la vie
		currentLife -= _damages;
        //le personnage ne peut plus attaquer
        canAttack = false;
        //si sa vie est un inférieure est 0
		if(currentLife <= 0)
		{
            //annoncer que le personnage est mort
			MyLog(Name+" est mort.");
			_fightManagerUpgraded.SomebodyIsDead(this);
		}
        //si il y a un attaquant, lui annoncer les dégats qu'il a infligé
		if(Attacker != null) Attacker.OpponentHurted(_damages,this);
	}

    //fonction permettant au personnage de savoir combien de dégâts il a infligé à sa cible
	public virtual void OpponentHurted(int _damages, CharacterUpgraded HurtedMan)
	{

	}

    //gestion du comportement des personnages en cas de mort d'un autre personnage
	public virtual void SomebodyIsDead(CharacterUpgraded deadMan)
	{
        //si le personnage est caché
		if(Hidden)
        {
            //on compte le nombre de personnages vivants
			int livingFighters = 0;
			for(int i = 0; i < _fightManagerUpgraded.charactersList.Count; i++)
			{
				CharacterUpgraded currentFighter = _fightManagerUpgraded.charactersList[i];
				if(currentFighter.currentLife > 0) livingFighters ++;
			}
            //si il y a moins de 5 combattants vivants
			if(livingFighters < 5)
			{
                //annuler le camouflage
				Reveal();
			}
		}
	}

    //fonction gérant l'administration des dégâts de poison
	public virtual void TakePoisonDamages()
	{
		MyLog(Name+" subis "+poisonLevel+" dégâts car il est intoxiqué");
        //diminution de la vie en focntion du niveau de poison
        currentLife -= poisonLevel;
        //si le personnage est mort
        if (currentLife <= 0)
		{
            //annoncer la mort du personnage
			MyLog(Name+" est mort.");
			_fightManagerUpgraded.SomebodyIsDead(this);
		}
	}

    //fonction gérant l'augmentation du niveau de poison
	public virtual void AugmentPoisonLevel(int _poisonInjection)
	{
		MyLog(Name+" s'intoxique.");
		poisonLevel += _poisonInjection;
	}

	//selectionner une cible valide
	public virtual void SelectTargetAndAttack()
	{
		target = null;
		//on cree une liste dans laquelle on stockera les cibles valides
		List<CharacterUpgraded> validTarget = new List<CharacterUpgraded>();

		for(int i = 0; i <  _fightManagerUpgraded.charactersList.Count; i++)
		{
			CharacterUpgraded currentCharacter =  _fightManagerUpgraded.charactersList[i];
			//si le personnga testé n'est pas celui qui attaque et qu'il est vivant et qu'il n'est pas camouflé
			if(currentCharacter != this && currentCharacter.currentLife > 0 && !currentCharacter.Hidden)
			{
				//on l'ajoute à la liste des cible valide
				validTarget.Add(currentCharacter);
			}
		}

		if(validTarget.Count > 0)
		{
			//on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque 
			target = validTarget[Random.Range(0,validTarget.Count)];
            //on lance l'attaque
            Attack();
		}
        //si on ne trouve pas de cible valide
		else
		{
			MyLog(Name+" n'a pas trouvé de cible valide");
            //on réduit le nombre d'attaque restant du persnnage à 0 pour éviter la boucle infinie
            CurrentAttackNumber = 0;
		}
	}

	//camoufler le personnage
	public virtual void Hide()
	{
		MyLog(Name+" se camoufle.");
		Hidden = true;
	}

	//réveler le personnage
	public virtual void Reveal()
	{
		MyLog(Name+" n'est plus camouflé.");
		Hidden = false;
	}

    //affiche les messages liés au personnage en utilisant sa couleur
	public void MyLog(string text)
	{
		Debug.Log("<color=#"+ColorUtility.ToHtmlStringRGB(myColor)+">"+text+"</color>");
	}

}
